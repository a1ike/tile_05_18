'use strict';

$(document).ready(function () {

  function muteMe(elem) {
    elem.muted = true;
    elem.pause();
  }

  // Try to mute all video and audio elements on the page
  function mutePage() {
    var videos = document.querySelectorAll('video'),
        audios = document.querySelectorAll('audio');

    [].forEach.call(videos, function (video) {
      muteMe(video);
    });
    [].forEach.call(audios, function (audio) {
      muteMe(audio);
    });
  }

  if ($(window).width() > 1200) {

    jQuery(function () {
      jQuery('#video').YTPlayer();
    });
  };

  $(window).resize(function () {
    if ($(window).width() > 1200) {

      jQuery(function () {
        jQuery('#video').YTPlayer();
      });
    }
  });

  $(document).on('scroll', function () {

    if ($(document).scrollTop() > 500) {
      $('.m-header').addClass('m-header_fixed');
    } else {
      $('.m-header').removeClass('m-header_fixed');
    }
  });

  /* $('a[href^="#"]').on('click', function (e) {
    e.preventDefault();
    var target = this.hash,
      $target = $(target);
     $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  }); */

  $('.phone').inputmask('+7(999)999-99-99');

  $('.m-goods__card-images1').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: false,
    fade: true,
    asNavFor: '.m-goods__card-colors1'
  });
  $('.m-goods__card-colors1').slick({
    slidesToShow: 11,
    slidesToScroll: 1,
    asNavFor: '.m-goods__card-images1',
    arrows: false,
    dots: false,
    centerMode: false,
    focusOnSelect: true,
    responsive: [{
      breakpoint: 1199,
      settings: {
        slidesToShow: 6,
        autoplay: true
      }
    }]
  });

  $('.m-goods__card-images2').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: false,
    fade: true,
    asNavFor: '.m-goods__card-colors2'
  });
  $('.m-goods__card-colors2').slick({
    slidesToShow: 11,
    slidesToScroll: 1,
    asNavFor: '.m-goods__card-images2',
    arrows: false,
    dots: false,
    centerMode: false,
    focusOnSelect: true,
    responsive: [{
      breakpoint: 1199,
      settings: {
        slidesToShow: 6,
        autoplay: true
      }
    }]
  });

  $('.m-goods__card-images3').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: false,
    fade: true,
    asNavFor: '.m-goods__card-colors3'
  });
  $('.m-goods__card-colors3').slick({
    slidesToShow: 11,
    slidesToScroll: 1,
    asNavFor: '.m-goods__card-images3',
    arrows: false,
    dots: false,
    centerMode: false,
    focusOnSelect: true,
    responsive: [{
      breakpoint: 1199,
      settings: {
        slidesToShow: 6,
        autoplay: true
      }
    }]
  });

  $('.m-reviews__cards').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: true,
    arrows: true,
    centerMode: false,
    autoplay: true,
    focusOnSelect: true,
    responsive: [{
      breakpoint: 1199,
      settings: {
        slidesToShow: 1
      }
    }]
  });

  $('.m-footer__slides').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    centerMode: false,
    autoplay: true
  });

  $('.m-stepper__toggler').on('click', function (e) {
    e.preventDefault();

    // Remove active class from buttons
    $('.m-stepper__toggler').removeClass('active');

    // Add the correct active class
    $(this).addClass('active');

    // Hide all tab items
    $('[class^=tab-item]').hide();

    // Show the clicked tab
    var num = $(this).data('tab-item');
    $('.tab-item-' + num).fadeIn();
  });

  $('.phone').inputmask('+7(999)999-99-99');

  var goodName = '';
  $('input[name="types"]').change(function () {
    if ($(this).val() === 'Старый город (1СГ5ф)') {
      goodName = 'old';
      $('#calc-color-image').attr('src', './images/goods/old/1.jpg');
    } else if ($(this).val() === 'Классика (1КЛ6ф)') {
      goodName = 'classic';
      $('#calc-color-image').attr('src', './images/goods/classic/1.jpg');
    } else if ($(this).val() === 'Газонный камень (1РД10)') {
      goodName = 'stone';
      $('#calc-color-image').attr('src', './images/goods/stone/1.jpg');
    }
  });

  $('input[name="colors"]').change(function () {
    if ($(this).val() === 'Серый') {
      $('#calc-color-image').attr('src', './images/goods/' + goodName + '/1.jpg');
    } else if ($(this).val() === 'Песочный') {
      $('#calc-color-image').attr('src', './images/goods/' + goodName + '/2.jpg');
    } else if ($(this).val() === 'Красно-коричневый') {
      $('#calc-color-image').attr('src', './images/goods/' + goodName + '/3.jpg');
    } else if ($(this).val() === 'Черный') {
      $('#calc-color-image').attr('src', './images/goods/' + goodName + '/4.jpg');
    } else if ($(this).val() === 'Красный') {
      $('#calc-color-image').attr('src', './images/goods/' + goodName + '/5.jpg');
    } else if ($(this).val() === 'Коричневый') {
      $('#calc-color-image').attr('src', './images/goods/' + goodName + '/6.jpg');
    } else if ($(this).val() === 'Бежевый') {
      $('#calc-color-image').attr('src', './images/goods/' + goodName + '/7.jpg');
    } else if ($(this).val() === 'Зеленый') {
      $('#calc-color-image').attr('src', './images/goods/' + goodName + '/8.jpg');
    } else if ($(this).val() === 'Синий') {
      $('#calc-color-image').attr('src', './images/goods/' + goodName + '/9.jpg');
    } else if ($(this).val() === 'Персиковый') {
      $('#calc-color-image').attr('src', './images/goods/' + goodName + '/10.jpg');
    } else if ($(this).val() === 'Желтый') {
      $('#calc-color-image').attr('src', './images/goods/' + goodName + '/11.jpg');
    }
    $('#calc-more-image').attr('src', $('#calc-color-image').attr('src'));
  });

  $('#final-take').on('click', function (e) {
    $('#calc-final-image').attr('src', $('#calc-color-image').attr('src'));

    var colors = $('input[name="colors"]').val();
    var cement = $('input[name="cement"]').val();
    var coloring = $('input[name="coloring"]').val();
    var services = '';
    if ($('input[name="services"]').is(':checked')) {
      services = 'Да';
    } else {
      services = 'Нет';
    };
    var services2 = '';
    if ($('input[name="services2"]').is(':checked')) {
      services2 = 'Да';
    } else {
      services2 = 'Нет';
    };
    var types = $('input[name="types"]').val();
    var price = '';
    if (types === 'Старый город (1СГ5ф)') {
      price = 433;
    } else if (types === 'Классика (1КЛ6ф)') {
      price = 454;
    } else if (types === 'Газонный камень (1РД10)') {
      price = 578;
    }

    $('#final-colors').text(colors);
    $('#final-cement').text(cement);
    $('#final-coloring').text(coloring);
    $('#final-services').text(services);
    $('#final-services2').text(services2);
    $('#final-types').text(types);
    $('#final-price').text(price);
  });

  $('.m-header__mob-nav').on('click', function (e) {
    e.preventDefault();
    $('.m-header__nav').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.open-modal-good').on('click', function (e) {
    var goodTitle = $(this).text();
    var goodSrc = '';

    if (goodTitle === 'Старый город (1СГ5ф)') {
      goodSrc = 'old';
      $('#good-image').attr('src', './images/goods/old/1.jpg');
    } else if (goodTitle === 'Классика (1КЛ6ф)') {
      goodSrc = 'classic';
      $('#good-image').attr('src', './images/goods/classic/1.jpg');
    } else if (goodTitle === 'Газонный камень (1РД10)') {
      goodSrc = 'stone';
      $('#good-image').attr('src', './images/goods/stone/1.jpg');
    }

    if (goodTitle === 'Старый город (1СГ5ф)') {
      $('#good-title').text('Старый город (1СГ5ф)');
      $('#good-height').text('50');
      $('#good-much1').text('12/12/12');
      $('#good-much2').text('12');
      $('#good-mass').text('1,4');
      $('#good-price1').text('433');
      $('#good-price2').text(Math.round(433 * 0.95));
    } else if (goodTitle === 'Классика (1КЛ6ф)') {
      $('#good-title').text('Классика (1КЛ6ф)');
      $('#good-height').text('60');
      $('#good-much1').text('96');
      $('#good-much2').text('9,4');
      $('#good-mass').text('1,4');
      $('#good-price1').text('454');
      $('#good-price2').text(Math.round(454 * 0.95));
    } else if (goodTitle === 'Газонный камень (1РД10)') {
      $('#good-title').text('Газонный камень (1РД10)');
      $('#good-height').text('100');
      $('#good-much1').text('4,16');
      $('#good-much2').text('9,60');
      $('#good-mass').text('1,38');
      $('#good-price1').text('578');
      $('#good-price2').text(Math.round(578 * 0.95));
    };

    $('input[name="good-color"]').change(function () {
      if ($(this).val() === 'Серый') {
        $('#good-image').attr('src', './images/goods/' + goodSrc + '/1.jpg');
      } else if ($(this).val() === 'Песочный') {
        $('#good-image').attr('src', './images/goods/' + goodSrc + '/2.jpg');
      } else if ($(this).val() === 'Красно-коричневый') {
        $('#good-image').attr('src', './images/goods/' + goodSrc + '/3.jpg');
      } else if ($(this).val() === 'Черный') {
        $('#good-image').attr('src', './images/goods/' + goodSrc + '/4.jpg');
      } else if ($(this).val() === 'Красный') {
        $('#good-image').attr('src', './images/goods/' + goodSrc + '/5.jpg');
      } else if ($(this).val() === 'Коричневый') {
        $('#good-image').attr('src', './images/goods/' + goodSrc + '/6.jpg');
      } else if ($(this).val() === 'Бежевый') {
        $('#good-image').attr('src', './images/goods/' + goodSrc + '/7.jpg');
      } else if ($(this).val() === 'Зеленый') {
        $('#good-image').attr('src', './images/goods/' + goodSrc + '/8.jpg');
      } else if ($(this).val() === 'Синий') {
        $('#good-image').attr('src', './images/goods/' + goodSrc + '/9.jpg');
      } else if ($(this).val() === 'Персиковый') {
        $('#good-image').attr('src', './images/goods/' + goodSrc + '/10.jpg');
      } else if ($(this).val() === 'Желтый') {
        $('#good-image').attr('src', './images/goods/' + goodSrc + '/11.jpg');
      }
    });

    $('#modal-good').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.close-modal-good').on('click', function (e) {
    $('#modal-good').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.open-modal-lider').on('click', function (e) {
    $('#modal-lider').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.close-modal-lider').on('click', function (e) {
    $('#modal-lider').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.open-modal-complete').on('click', function (e) {
    $('#modal-complete').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.close-modal-complete').on('click', function (e) {
    $('#modal-complete').slideToggle('fast', function (e) {
      // callback
    });
    window.open('http://mos-bruschatka.ru/price.pdf', '_blank');
  });

  $('#form-liders').submit(function (e) {
    e.preventDefault();
    var f = $(this);
    $('.ierror', f).removeClass('ierror');
    var person = $('input[name="person"]', f).val();
    var phone = $('input[name="phone"]', f).val();
    var error = false;
    if (error) {
      return false;
    }
    $.ajax({
      type: 'POST',
      url: 'mail-liders.php',
      data: $(this).serialize()
    }).done(function (data) {
      if (data.response == 'ok') {
        $('#modal-lider').slideToggle('fast', function (e) {
          // callback
        });
        $('#modal-complete').slideToggle('fast', function (e) {
          // callback
        });
      } else {
        alert('Ошибка, попробуйте повторить позже!');
      }
    });
    return false;
  });

  $('#form-footer').submit(function (e) {
    e.preventDefault();
    var f = $(this);
    $('.ierror', f).removeClass('ierror');
    var phone = $('input[name="phone"]', f).val();
    var error = false;
    if (error) {
      return false;
    }
    $.ajax({
      type: 'POST',
      url: 'mail-footer.php',
      data: $(this).serialize()
    }).done(function (data) {
      if (data.response == 'ok') {
        alert('Спасибо, ваша заявка отправлена!');
      } else {
        alert('Ошибка, попробуйте повторить позже!');
      }
    });
    return false;
  });

  $('#form-complete').submit(function (e) {
    e.preventDefault();
    var f = $(this);
    $('.ierror', f).removeClass('ierror');
    var phone = $('input[name="phone"]', f).val();
    var error = false;
    if (error) {
      return false;
    }
    $.ajax({
      type: 'POST',
      url: 'mail-complete.php',
      data: $(this).serialize()
    }).done(function (data) {
      if (data.response == 'ok') {
        $('#modal-complete').slideToggle('fast', function (e) {
          // callback
        });
        window.open('http://mos-bruschatka.ru/price.pdf', '_blank');
      } else {
        alert('Ошибка, попробуйте повторить позже!');
      }
    });
    return false;
  });

  $('#form-calc').submit(function (e) {
    e.preventDefault();
    var f = $(this);
    $('.ierror', f).removeClass('ierror');
    var colors = $('input[name="colors"]').val();
    var cement = $('input[name="cement"]').val();
    var coloring = $('input[name="coloring"]').val();
    var services = $('input[name="services"]').val();
    var services2 = $('input[name="services2"]').val();
    var types = $('input[name="types"]').val();
    var phone = $('input[name="calc-phone"]', f).val();
    var price = $('#final-price').text();
    var error = false;
    if (error) {
      return false;
    }
    $.ajax({
      type: 'POST',
      url: 'mail-calc.php',
      data: $(this).serialize()
    }).done(function (data) {
      if (data.response == 'ok') {
        alert('Спасибо! Ваша заявка отправлена');
      } else {
        alert('Ошибка, попробуйте повторить позже!');
      }
    });
    return false;
  });
});