<?php
####################
# API manual: http://registrator.leadvertex.info/webmaster/api.html
####################
final class Leadvertex
{
	protected $_advertiser_id;
	protected $_webmaster_id;
	protected $_api_token;
	public $error = null;

	function __construct($advertiser_id = '', $webmaster_id = '', $api_token = '') {
		$this->_advertiser_id = $advertiser_id;
		$this->_webmaster_id = $webmaster_id;
		$this->_api_token = $api_token;
	}

	protected function _request($api_method, $http_method, $data) {
		$url = 'https://'.$this->_advertiser_id.'.leadvertex.ru/api/webmaster/v2/'.$api_method.'.html?webmasterID='.$this->_webmaster_id.'&token='.$this->_api_token;
		if($http_method == 'GET') {
			$url .= '&'.http_build_query($data);
			$data = array();
		}

		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CONNECTTIMEOUT => 60,
			CURLOPT_TIMEOUT => 60,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_CUSTOMREQUEST => $http_method,
			CURLOPT_POSTFIELDS => http_build_query($data),
		));
		$response = json_decode(curl_exec($curl));
		curl_close($curl);
		return $response;
	}

	public function add_lead($referer, $remote_ip, $phone, $name = 'Уточнить у клиента', $price = '0') {
		# Сборка ссылки на лэнд без query параметров
		$temp = parse_url($referer);
		$domain = isset($temp['host']) ? $temp['host'] : 'NO DOMAIN GET';
		parse_str(isset($temp['query']) ? $temp['query'] : '', $get);

		$utm_source = empty($get['utm_source']) ? '' : $get['utm_source'];
		$utm_medium = empty($get['utm_medium']) ? '' : $get['utm_medium'];
		$utm_campaign = empty($get['utm_campaign']) ? '' : $get['utm_campaign'];
		$utm_content = empty($get['utm_content']) ? '' : $get['utm_content'];
		$utm_term = empty($get['utm_term']) ? '' : $get['utm_term'];

		# Подготовка данных по заказу
		$order_data = array(
			'country' => '',				# страна. String[70]
			'region' => '',					# регион. String[255]
			'city' => '',					# город. String[255]
			'postIndex' => '',				# почтовый индекс. String[6]
			'address' => '',				# адрес. String[255]
			'house' => '',					# дом/строение. String[10]
			'flat' => '',					# квартира/офис. String[10]
			'fio' => $name,					# Ф.И.О. String[255]
			'phone' => $phone,				# телефон. String[25]
			'email' => '',					# email. String[255]
			'price' => floatval($price),	# цена за единицу товара. FLOAT
			'total' => floatval($price),	# цена итого. FLOAT
			'quantity' => '1',				# количество единиц товара. INT
			'additional1' => '',			# дополнительное поле 1. String[255] order_id в системе партнёра
			'additional2' => '',			# дополнительное поле 2. String[255] имя оффера в системе партнёра
			'additional3' => '',			# дополнительное поле 3. String[255]
			'additional4' => '',			# дополнительное поле 4. String[255]
			'additional5' => '',			# дополнительное поле 5. String[255] ид вебмастера
			'additional6' => '',			# дополнительное поле 6. String[255]
			'additional7' => '',			# дополнительное поле 7. String[255]
			'additional8' => '',			# дополнительное поле 8. String[255]
			'additional9' => '',			# дополнительное поле 9. String[255]
			'additional10' => '',			# дополнительное поле 10. String[255]
			'additional11' => '',			# дополнительное поле 11. String[500]
			'additional12' => '',			# дополнительное поле 12. String[500]
			'additional13' => '',			# дополнительное поле 13. String[500]
			'additional14' => '',			# дополнительное поле 14. String[500]
			'additional15' => '',			# дополнительное поле 15. String[500]
			'comment' => '',				# комментарий к заказу. String[500]
			'russianpostTrack' => '',		# трек-номер Почты России. String[14]
			'novaposhtaTrack' => '',		# трек-номер Нова Пошта. String[14]
			'kazpostTrack' => '',			# трек-номер Казпочты. String[14]
			'timezone' => '',				# целое число. Смещение времени клиента в минутах относительно UTC (чтобы не звонить ему ночью). INT
			'utm_source' => $utm_source,	# метка 1. String[100]
			'utm_medium' => $utm_medium,	# метка 2. String[100]
			'utm_campaign' => $utm_campaign,# метка 3. String[100]
			'utm_term' => $utm_term,		# метка 4. String[100]
			'utm_content' => $utm_content,	# метка 5. String[100]
			'domain' => $domain,			# домен, на котором был совершен заказ. String[255]
			'timeSpent' => '',				# Затрачено времени на заказ в секундах. INT
			'ip' => $remote_ip,				# ip клиента, сделавшего заказ. STRING[15]
			'referer' => '',				# referer клиента, сделавшего заказ. STRING[200]
			'goods' => array(				# Товары, задаются в массиве вида
				array(
					'goodID' => '1',
					'quantity' => '1',
					'price' => $price,
				),
			),
		);

		$response = $this->_request('addOrder', 'POST', $order_data);

		if(empty($response->OK)) {
			$this->error = 'error: '.json_encode($response);
			return false;
		}
		return $response->OK;
	}
}
